# About this document
## Omeka Classic
This document is a manual for Omeka Classic. If you are looking for information about Omeka S, please go to https://omeka.org/s/ (EN) or https://omeka.fr/ (FR).

## How to modify?
- If you want to add comments or ideas, do not hesitate to [create an issue](https://gitlab.huma-num.fr/elan-numerique/manuels/omeka-autoformation/-/issues) (check before if your idea is not already described ;-))
- If you want to modify the source: <a href="mailto:annegf@univ-grenoble-alpes.fr,camille.desiles@univ-grenoble-alpes.fr?subject=Ask for access to Overleaf/Omeka-autoformation">ask for access</a> and see you on [overleaf](https://www.overleaf.com/project)!

## Cite us
> Camille Desiles, Anne Garcia Fernandez, « Autoformation à Omeka Classic », version 1.1 (17 août 2021), 2021. Source : https://gitlab.huma-num.fr/elan-numerique/manuels/omeka-autoformation/-/releases/1.1



# Latex Template
## Sleek Template

Sleek Template is a minimal collection of [LaTeX](https://www.latex-project.org/) packages and settings that ease the writing of beautiful documents. While originally meant for theses, it is perfectly suitable for project reports, articles, syntheses, etc. – with a few adjustments, like margins.

It is composed of four separate packages – `sleek`, `sleek-title`, `sleek-theorems` and `sleek-listings` – each of which can be used individually.

For more information about the packages, settings, environments, commands, etc., please refer to the documentation file [`main.pdf`](main.pdf), which also acts as showcase document.

## Overleaf :leaves:

Sleek Template is [Overleaf](https://www.overleaf.com/) ready!

1. Download the repository [archive](https://github.com/francois-rozet/sleek-template/archive/overleaf.zip)
2. On your Overleaf project page, click **New Project** and choose **Upload Project**
3. Drag or select the downloaded archive
4. Rename the project
5. Enjoy :ok_hand:

## Author

* **François Rozet** - [francois-rozet](https://github.com/francois-rozet)
